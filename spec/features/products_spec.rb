require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:product) { create(:product) }

  before do
    visit potepan_product_path(product.id)
  end

  scenario "商品のテンプレート表示が正しいこと" do
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_content product.description
  end
  scenario "home画面へ移動すること" do
    click_on 'BIGBAG'
    expect(page).to have_content "人気カテゴリー", "新着商品"
  end
end
